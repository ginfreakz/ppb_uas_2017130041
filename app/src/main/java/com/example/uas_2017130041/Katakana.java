package com.example.uas_2017130041;

import java.util.Random;

public class Katakana {
    private int listPertanyaan[] = {
            R.drawable.katakana_a,
            R.drawable.katakana_i,
            R.drawable.katakana_u,
            R.drawable.katakana_e,
            R.drawable.katakana_o,
            R.drawable.katakana_ka,
            R.drawable.katakana_ki,
            R.drawable.katakana_ku,
            R.drawable.katakana_ke,
            R.drawable.katakana_ko,
            R.drawable.katakana_sa,
            R.drawable.katakana_shi,
            R.drawable.katakana_su,
            R.drawable.katakana_se,
            R.drawable.katakana_so,
            R.drawable.katakana_ta,
            R.drawable.katakana_chi,
            R.drawable.katakana_tsu,
            R.drawable.katakana_te,
            R.drawable.katakana_to,
            R.drawable.katakana_na,
            R.drawable.katakana_ni,
            R.drawable.katakana_nu,
            R.drawable.katakana_ne,
            R.drawable.katakana_no,
    };
    private int listJawaban[] = {
            R.drawable.huruf_a,
            R.drawable.huruf_i,
            R.drawable.huruf_u,
            R.drawable.huruf_e,
            R.drawable.huruf_o,
            R.drawable.huruf_ka,
            R.drawable.huruf_ki,
            R.drawable.huruf_ku,
            R.drawable.huruf_ke,
            R.drawable.huruf_ko,
            R.drawable.huruf_sa,
            R.drawable.huruf_shi,
            R.drawable.huruf_su,
            R.drawable.huruf_se,
            R.drawable.huruf_so,
            R.drawable.huruf_ta,
            R.drawable.huruf_chi,
            R.drawable.huruf_tsu,
            R.drawable.huruf_te,
            R.drawable.huruf_to,
            R.drawable.huruf_na,
            R.drawable.huruf_ni,
            R.drawable.huruf_nu,
            R.drawable.huruf_ne,
            R.drawable.huruf_no,
    };

    public int[] getlist(){
        return listPertanyaan;
    }
    public int[] getlist2(){
        return listJawaban;
    }

    public int getRandom(){
        int rnd = new Random().nextInt(listPertanyaan.length);
        return rnd;
    }
    public int getImageSoal(int i){
        return  listPertanyaan[i];
    }
    public int getImageJawaban(int i){
        return  listJawaban[i];
    }
    public int getJumlah(){
        return listPertanyaan.length;
    }
    public int getJumlah2(){
        return listJawaban.length;
    }
}
