package com.example.uas_2017130041;

import java.util.Random;

public class Hiragana {
    private int listPertanyaan[] = {
            R.drawable.hiragana_a,
            R.drawable.hiragana_i,
            R.drawable.hiragana_u,
            R.drawable.hiragana_e,
            R.drawable.hiragana_o,
            R.drawable.hiragana_ka,
            R.drawable.hiragana_ki,
            R.drawable.hiragana_ku,
            R.drawable.hiragana_ke,
            R.drawable.hiragana_ko,
            R.drawable.hiragana_sa,
            R.drawable.hiragana_shi,
            R.drawable.hiragana_su,
            R.drawable.hiragana_se,
            R.drawable.hiragana_so,
            R.drawable.hiragana_ta,
            R.drawable.hiragana_chi,
            R.drawable.hiragana_tsu,
            R.drawable.hiragana_te,
            R.drawable.hiragana_to,
            R.drawable.hiragana_na,
            R.drawable.hiragana_ni,
            R.drawable.hiragana_nu,
            R.drawable.hiragana_ne,
            R.drawable.hiragana_no,
    };
    private int listJawaban[] = {
            R.drawable.huruf_a,
            R.drawable.huruf_i,
            R.drawable.huruf_u,
            R.drawable.huruf_e,
            R.drawable.huruf_o,
            R.drawable.huruf_ka,
            R.drawable.huruf_ki,
            R.drawable.huruf_ku,
            R.drawable.huruf_ke,
            R.drawable.huruf_ko,
            R.drawable.huruf_sa,
            R.drawable.huruf_shi,
            R.drawable.huruf_su,
            R.drawable.huruf_se,
            R.drawable.huruf_so,
            R.drawable.huruf_ta,
            R.drawable.huruf_chi,
            R.drawable.huruf_tsu,
            R.drawable.huruf_te,
            R.drawable.huruf_to,
            R.drawable.huruf_na,
            R.drawable.huruf_ni,
            R.drawable.huruf_nu,
            R.drawable.huruf_ne,
            R.drawable.huruf_no,
    };

    public int[] getlist(){
        return listPertanyaan;
    }
    public int[] getlist2(){
        return listJawaban;
    }

    public int getRandom(){
        int rnd = new Random().nextInt(listPertanyaan.length);
        return rnd;
    }
    public int getImageSoal(int i){
        return  listPertanyaan[i];
    }
    public int getImageJawaban(int i){
        return  listJawaban[i];
    }
    public int getJumlah(){
        return listPertanyaan.length;
    }
    public int getJumlah2(){
        return listJawaban.length;
    }
}
