package com.example.uas_2017130041;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;

public class HiraganaActivity extends AppCompatActivity {
    ImageView TampilGambar;
    ImageButton show,hide,pindah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hiragana);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TampilGambar = (ImageView) findViewById(R.id.popup);
        show = (ImageButton)findViewById(R.id.a);
        hide = (ImageButton)findViewById(R.id.i);

        final Animation animScale = AnimationUtils.loadAnimation(this, R.anim.anime_scale);

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setVisibility(View.VISIBLE);
            }
        });

        hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setVisibility(View.INVISIBLE);
            }
        });

        final MediaPlayer suaraA = MediaPlayer.create(this, R.raw.a);
        final MediaPlayer suaraI = MediaPlayer.create(this, R.raw.i);
        final MediaPlayer suaraU = MediaPlayer.create(this, R.raw.u);
        final MediaPlayer suaraE = MediaPlayer.create(this, R.raw.e);
        final MediaPlayer suaraO = MediaPlayer.create(this, R.raw.o);
        final MediaPlayer suaraKa = MediaPlayer.create(this, R.raw.ka);
        final MediaPlayer suaraKi = MediaPlayer.create(this, R.raw.ki);
        final MediaPlayer suaraKu = MediaPlayer.create(this, R.raw.ku);
        final MediaPlayer suaraKe = MediaPlayer.create(this, R.raw.ke);
        final MediaPlayer suaraKo = MediaPlayer.create(this, R.raw.ko);
        final MediaPlayer suaraSa = MediaPlayer.create(this, R.raw.sa);
        final MediaPlayer suaraShi = MediaPlayer.create(this, R.raw.shi);
        final MediaPlayer suaraSu = MediaPlayer.create(this, R.raw.su);
        final MediaPlayer suaraSe = MediaPlayer.create(this, R.raw.se);
        final MediaPlayer suaraSo = MediaPlayer.create(this, R.raw.so);
        final MediaPlayer suaraTa = MediaPlayer.create(this, R.raw.ta);
        final MediaPlayer suaraChi = MediaPlayer.create(this, R.raw.chi);
        final MediaPlayer suaraTsu = MediaPlayer.create(this, R.raw.tsu);
        final MediaPlayer suaraTe = MediaPlayer.create(this, R.raw.te);
        final MediaPlayer suaraTo = MediaPlayer.create(this, R.raw.to);
        final MediaPlayer suaraNa = MediaPlayer.create(this, R.raw.na);
        final MediaPlayer suaraNi = MediaPlayer.create(this, R.raw.ni);
        final MediaPlayer suaraNu = MediaPlayer.create(this, R.raw.nu);
        final MediaPlayer suaraNe = MediaPlayer.create(this, R.raw.ne);
        final MediaPlayer suaraNo = MediaPlayer.create(this, R.raw.no);


        ImageButton ButtonSuara = (ImageButton)this.findViewById(R.id.a);
        ImageButton ButtonSuara2 = (ImageButton)this.findViewById(R.id.i);
        ImageButton ButtonSuara3 = (ImageButton)this.findViewById(R.id.u);
        ImageButton ButtonSuara4 = (ImageButton)this.findViewById(R.id.e);
        ImageButton ButtonSuara5 = (ImageButton)this.findViewById(R.id.o);
        ImageButton ButtonSuara6 = (ImageButton)this.findViewById(R.id.ka);
        ImageButton ButtonSuara7 = (ImageButton)this.findViewById(R.id.ki);
        ImageButton ButtonSuara8 = (ImageButton)this.findViewById(R.id.ku);
        ImageButton ButtonSuara9 = (ImageButton)this.findViewById(R.id.ke);
        ImageButton ButtonSuara10 = (ImageButton)this.findViewById(R.id.ko);
        ImageButton ButtonSuara11 = (ImageButton)this.findViewById(R.id.sa);
        ImageButton ButtonSuara12 = (ImageButton)this.findViewById(R.id.shi);
        ImageButton ButtonSuara13 = (ImageButton)this.findViewById(R.id.su);
        ImageButton ButtonSuara14 = (ImageButton)this.findViewById(R.id.se);
        ImageButton ButtonSuara15 = (ImageButton)this.findViewById(R.id.so);
        ImageButton ButtonSuara16 = (ImageButton)this.findViewById(R.id.ta);
        ImageButton ButtonSuara17 = (ImageButton)this.findViewById(R.id.chi);
        ImageButton ButtonSuara18 = (ImageButton)this.findViewById(R.id.tsu);
        ImageButton ButtonSuara19 = (ImageButton)this.findViewById(R.id.te);
        ImageButton ButtonSuara20 = (ImageButton)this.findViewById(R.id.to);
        ImageButton ButtonSuara21 = (ImageButton)this.findViewById(R.id.na);
        ImageButton ButtonSuara22 = (ImageButton)this.findViewById(R.id.ni);
        ImageButton ButtonSuara23 = (ImageButton)this.findViewById(R.id.nu);
        ImageButton ButtonSuara24 = (ImageButton)this.findViewById(R.id.ne);
        ImageButton ButtonSuara25 = (ImageButton)this.findViewById(R.id.no);


        ButtonSuara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.a);
                TampilGambar.startAnimation(animScale);
                suaraA.start();
            }
        });

        ButtonSuara2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.i);
                TampilGambar.startAnimation(animScale);
                suaraI.start();
            }
        });

        ButtonSuara3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.u);
                TampilGambar.startAnimation(animScale);
                suaraU.start();
            }
        });

        ButtonSuara4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.e);
                TampilGambar.startAnimation(animScale);
                suaraE.start();
            }
        });

        ButtonSuara5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.o);
                TampilGambar.startAnimation(animScale);
                suaraO.start();
            }
        });

        ButtonSuara6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.ka);
                TampilGambar.startAnimation(animScale);
                suaraKa.start();
            }
        });

        ButtonSuara7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.ki);
                TampilGambar.startAnimation(animScale);
                suaraKi.start();
            }
        });

        ButtonSuara8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.ku);
                TampilGambar.startAnimation(animScale);
                suaraKu.start();
            }
        });

        ButtonSuara9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.ke);
                TampilGambar.startAnimation(animScale);
                suaraKe.start();
            }
        });

        ButtonSuara10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.ko);
                TampilGambar.startAnimation(animScale);
                suaraKo.start();
            }
        });

        ButtonSuara11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.sa);
                TampilGambar.startAnimation(animScale);
                suaraSa.start();
            }
        });

        ButtonSuara12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.shi);
                TampilGambar.startAnimation(animScale);
                suaraShi.start();
            }
        });

        ButtonSuara13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.su);
                TampilGambar.startAnimation(animScale);
                suaraSu.start();
            }
        });

        ButtonSuara14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.se);
                TampilGambar.startAnimation(animScale);
                suaraSe.start();
            }
        });

        ButtonSuara15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.so);
                TampilGambar.startAnimation(animScale);
                suaraSo.start();
            }
        });

        ButtonSuara16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.ta);
                TampilGambar.startAnimation(animScale);
                suaraTa.start();
            }
        });

        ButtonSuara17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.chi);
                TampilGambar.startAnimation(animScale);
                suaraChi.start();
            }
        });

        ButtonSuara18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.tsu);
                TampilGambar.startAnimation(animScale);
                suaraTsu.start();
            }
        });

        ButtonSuara19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.te);
                TampilGambar.startAnimation(animScale);
                suaraTe.start();
            }
        });

        ButtonSuara20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.to);
                TampilGambar.startAnimation(animScale);
                suaraTo.start();
            }
        });

        ButtonSuara21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.na);
                TampilGambar.startAnimation(animScale);
                suaraNa.start();
            }
        });

        ButtonSuara22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.ni);
                TampilGambar.startAnimation(animScale);
                suaraNi.start();
            }
        });

        ButtonSuara23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.nu);
                TampilGambar.startAnimation(animScale);
                suaraNu.start();
            }
        });

        ButtonSuara24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.ne);
                TampilGambar.startAnimation(animScale);
                suaraNe.start();
            }
        });

        ButtonSuara25.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.no);
                TampilGambar.startAnimation(animScale);
                suaraNo.start();
            }
        });
    }
}