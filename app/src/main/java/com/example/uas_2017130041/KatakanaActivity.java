package com.example.uas_2017130041;

import androidx.appcompat.app.AppCompatActivity;

import android.media.ImageWriter;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;

public class KatakanaActivity extends AppCompatActivity {
    ImageView TampilGambar;
    ImageButton show,hide,pindah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_katakana);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TampilGambar = (ImageView) findViewById(R.id.popup_a);
        show = (ImageButton)findViewById(R.id.kat_a);
        hide = (ImageButton)findViewById(R.id.kat_i);

        final Animation animScale = AnimationUtils.loadAnimation(this, R.anim.anime_scale);

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setVisibility(View.VISIBLE);
            }
        });

        hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setVisibility(View.INVISIBLE);
            }
        });

        final MediaPlayer suaraKata = MediaPlayer.create(this, R.raw.a);
        final MediaPlayer suaraKati = MediaPlayer.create(this, R.raw.i);
        final MediaPlayer suaraKatu = MediaPlayer.create(this, R.raw.u);
        final MediaPlayer suaraKate = MediaPlayer.create(this, R.raw.e);
        final MediaPlayer suaraKato = MediaPlayer.create(this, R.raw.o);
        final MediaPlayer suaraKatka = MediaPlayer.create(this, R.raw.ka);
        final MediaPlayer suaraKatki = MediaPlayer.create(this, R.raw.ki);
        final MediaPlayer suaraKatku = MediaPlayer.create(this, R.raw.ku);
        final MediaPlayer suaraKatke = MediaPlayer.create(this, R.raw.ke);
        final MediaPlayer suaraKatko = MediaPlayer.create(this, R.raw.ko);
        final MediaPlayer suaraKatsa = MediaPlayer.create(this, R.raw.sa);
        final MediaPlayer suaraKatshi = MediaPlayer.create(this, R.raw.shi);
        final MediaPlayer suaraKatsu = MediaPlayer.create(this, R.raw.su);
        final MediaPlayer suaraKatse = MediaPlayer.create(this, R.raw.se);
        final MediaPlayer suaraKatso = MediaPlayer.create(this, R.raw.so);
        final MediaPlayer suaraKatta = MediaPlayer.create(this, R.raw.ta);
        final MediaPlayer suaraKatchi = MediaPlayer.create(this, R.raw.chi);
        final MediaPlayer suaraKattsu = MediaPlayer.create(this, R.raw.tsu);
        final MediaPlayer suaraKatte = MediaPlayer.create(this, R.raw.te);
        final MediaPlayer suaraKatto = MediaPlayer.create(this, R.raw.to);
        final MediaPlayer suaraKatna = MediaPlayer.create(this, R.raw.na);
        final MediaPlayer suaraKatni = MediaPlayer.create(this, R.raw.ni);
        final MediaPlayer suaraKatnu = MediaPlayer.create(this, R.raw.nu);
        final MediaPlayer suaraKatne = MediaPlayer.create(this, R.raw.ne);
        final MediaPlayer suaraKatno = MediaPlayer.create(this, R.raw.no);

        ImageButton ButtonSuara = (ImageButton)this.findViewById(R.id.kat_a);
        ImageButton ButtonSuara2 = (ImageButton)this.findViewById(R.id.kat_i);
        ImageButton ButtonSuara3 = (ImageButton)this.findViewById(R.id.kat_u);
        ImageButton ButtonSuara4 = (ImageButton)this.findViewById(R.id.kat_e);
        ImageButton ButtonSuara5 = (ImageButton)this.findViewById(R.id.kat_o);
        ImageButton ButtonSuara6 = (ImageButton)this.findViewById(R.id.kat_ka);
        ImageButton ButtonSuara7 = (ImageButton)this.findViewById(R.id.kat_ki);
        ImageButton ButtonSuara8 = (ImageButton)this.findViewById(R.id.kat_ku);
        ImageButton ButtonSuara9 = (ImageButton)this.findViewById(R.id.kat_ke);
        ImageButton ButtonSuara10 = (ImageButton)this.findViewById(R.id.kat_ko);
        ImageButton ButtonSuara11 = (ImageButton)this.findViewById(R.id.kat_sa);
        ImageButton ButtonSuara12 = (ImageButton)this.findViewById(R.id.kat_shi);
        ImageButton ButtonSuara13 = (ImageButton)this.findViewById(R.id.kat_su);
        ImageButton ButtonSuara14 = (ImageButton)this.findViewById(R.id.kat_se);
        ImageButton ButtonSuara15 = (ImageButton)this.findViewById(R.id.kat_so);
        ImageButton ButtonSuara16 = (ImageButton)this.findViewById(R.id.kat_ta);
        ImageButton ButtonSuara17 = (ImageButton)this.findViewById(R.id.kat_chi);
        ImageButton ButtonSuara18 = (ImageButton)this.findViewById(R.id.kat_tsu);
        ImageButton ButtonSuara19 = (ImageButton)this.findViewById(R.id.kat_te);
        ImageButton ButtonSuara20 = (ImageButton)this.findViewById(R.id.kat_to);
        ImageButton ButtonSuara21 = (ImageButton)this.findViewById(R.id.kat_na);
        ImageButton ButtonSuara22 = (ImageButton)this.findViewById(R.id.kat_ni);
        ImageButton ButtonSuara23 = (ImageButton)this.findViewById(R.id.kat_nu);
        ImageButton ButtonSuara24 = (ImageButton)this.findViewById(R.id.kat_ne);
        ImageButton ButtonSuara25 = (ImageButton)this.findViewById(R.id.kat_no);

        ButtonSuara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_a);
                TampilGambar.startAnimation(animScale);
                suaraKata.start();
            }
        });

        ButtonSuara2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_i);
                TampilGambar.startAnimation(animScale);
                suaraKati.start();
            }
        });

        ButtonSuara3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_u);
                TampilGambar.startAnimation(animScale);
                suaraKatu.start();
            }
        });

        ButtonSuara4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_e);
                TampilGambar.startAnimation(animScale);
                suaraKate.start();
            }
        });

        ButtonSuara5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_o);
                TampilGambar.startAnimation(animScale);
                suaraKato.start();
            }
        });

        ButtonSuara6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_ka);
                TampilGambar.startAnimation(animScale);
                suaraKatka.start();
            }
        });

        ButtonSuara7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_ki);
                TampilGambar.startAnimation(animScale);
                suaraKatki.start();
            }
        });

        ButtonSuara8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_ku);
                TampilGambar.startAnimation(animScale);
                suaraKatku.start();
            }
        });

        ButtonSuara9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_ke);
                TampilGambar.startAnimation(animScale);
                suaraKatke.start();
            }
        });

        ButtonSuara10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_ko);
                TampilGambar.startAnimation(animScale);
                suaraKatko.start();
            }
        });

        ButtonSuara11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_sa);
                TampilGambar.startAnimation(animScale);
                suaraKatsa.start();
            }
        });

        ButtonSuara12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_shi);
                TampilGambar.startAnimation(animScale);
                suaraKatshi.start();
            }
        });

        ButtonSuara13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_su);
                TampilGambar.startAnimation(animScale);
                suaraKatsu.start();
            }
        });

        ButtonSuara14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_se);
                TampilGambar.startAnimation(animScale);
                suaraKatse.start();
            }
        });

        ButtonSuara15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_so);
                TampilGambar.startAnimation(animScale);
                suaraKatso.start();
            }
        });

        ButtonSuara16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_ta);
                TampilGambar.startAnimation(animScale);
                suaraKatta.start();
            }
        });

        ButtonSuara17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_chi);
                TampilGambar.startAnimation(animScale);
                suaraKatchi.start();
            }
        });

        ButtonSuara18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_tsu);
                TampilGambar.startAnimation(animScale);
                suaraKattsu.start();
            }
        });

        ButtonSuara19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_te);
                TampilGambar.startAnimation(animScale);
                suaraKatte.start();
            }
        });

        ButtonSuara20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_to);
                TampilGambar.startAnimation(animScale);
                suaraKatto.start();
            }
        });

        ButtonSuara21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_na);
                TampilGambar.startAnimation(animScale);
                suaraKatna.start();
            }
        });

        ButtonSuara22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_ni);
                TampilGambar.startAnimation(animScale);
                suaraKatni.start();
            }
        });

        ButtonSuara23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_nu);
                TampilGambar.startAnimation(animScale);
                suaraKatnu.start();
            }
        });

        ButtonSuara24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_ne);
                TampilGambar.startAnimation(animScale);
                suaraKatne.start();
            }
        });

        ButtonSuara25.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TampilGambar.setImageResource(R.drawable.kat_no);
                TampilGambar.startAnimation(animScale);
                suaraKatno.start();
            }
        });
    }
}