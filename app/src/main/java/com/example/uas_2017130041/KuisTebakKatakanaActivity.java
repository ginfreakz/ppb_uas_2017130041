package com.example.uas_2017130041;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class KuisTebakKatakanaActivity extends AppCompatActivity {
    ImageButton pilih;
    ImageButton jawaban1,jawaban2,jawaban3;
    ImageView soal;
    int s,s1,j1,j2,j3;
    int skor = 0;

    Katakana katakana = new Katakana();
    int n = katakana.getJumlah();

    boolean jawabanBenar = true;
    int i = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kuis_tebak_katakana);

        soal = (ImageView)findViewById(R.id.soal);
        jawaban1 = (ImageButton)findViewById(R.id.jawaban1);
        jawaban2 = (ImageButton)findViewById(R.id.jawaban2);
        jawaban3 = (ImageButton)findViewById(R.id.jawaban3);

        final MediaPlayer SuaraButton = MediaPlayer.create(this,R.raw.klik);
        newLevel();

        jawaban1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCorrect(j1 == s);
            }
        });

        jawaban2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCorrect(j2 == s);
            }
        });

        jawaban3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCorrect(j3 == s);
            }
        });
    }

    public void newLevel(){
        s = katakana.getRandom();
        s1 = katakana.getRandom();
        int i = new Random().nextInt(3) + 1 ;

        if(i==1){
            j1 = s;
        }
        else{
            j1 = katakana.getRandom();
        }
        if (i==2){
            j2 = s;
        }
        else{
            j2 = katakana.getRandom();
        }
        if (i==3){
            j3 = s;
        }
        else{
            j3 = katakana.getRandom();
        }

        soal.setBackgroundResource(katakana.getImageSoal(s));
        jawaban1.setBackgroundResource(katakana.getImageJawaban(j1));
        jawaban2.setBackgroundResource(katakana.getImageJawaban(j2));
        jawaban3.setBackgroundResource(katakana.getImageJawaban(j3));
    }
    public void isCorrect(boolean input){
        TextView tampilSkor = (TextView)findViewById(R.id.skor);

        if(input && 1 < n){
            MediaPlayer benar;
            benar = MediaPlayer.create(getBaseContext(),R.raw.correct);
            skor += 10;
            benar.start();
            newLevel();
            i++;
        }else{
            MediaPlayer salah;
            salah = MediaPlayer.create(getBaseContext(),R.raw.wrong);
            skor -= 5;
            salah.start();
        }
        tampilSkor.setText("SKOR = " + skor);
    }
}