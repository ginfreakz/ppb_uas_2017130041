package com.example.uas_2017130041;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class KuisActivity extends AppCompatActivity {
ImageButton pindah;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kuis);

        pindah = (ImageButton)findViewById(R.id.menu_kuis_Hiragana);
        pindah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(KuisActivity.this,KuisTebakHiraganaActivity.class);
                startActivity(intent);
            }
        });

        pindah = (ImageButton)findViewById(R.id.menu_kuis_Katakana);
        pindah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(KuisActivity.this,KuisTebakKatakanaActivity.class);
                startActivity(intent);
            }
        });
    }
}